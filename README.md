# Certification application **Dragon**

## Abbreviations

- Certification &rightarrow; **cert**
- Back-end &rightarrow; **BE**
- Front-end &rightarrow; **FE**
- Checklist/todo/form &rightarrow; **form**

## Description


grejer e paj

This application is for creating, removing and updating **cert** (ISO?...). A user should be able to see a list of all created **certs** or search and then get a list of relevant **certs**.
When a user has clicked on **cert** another view is shown. Where a image and description of the selected **cert**. In this view there is also a **form** for the user to go through. When user is done, user can submit this **form** list and add store/company or dragon owner where this inspection is done.

## Nice to have links

[udemy](https://www.udemy.com/)

[Pluralsight](https://www.pluralsight.com/)

## TODO for **CERT** application Dragon

### **BE**

- [ ] create
- [ ] remove
- [ ] update
- [ ] get all
- [ ] search
- [ ] submit form

### **FE**

- [ ] create
- [ ] remove
- [ ] update
- [ ] show all
- [ ] search
- [ ] submit form
  - [ ] create **form**
  - [ ] show description and image
- [ ] work offline <!--- https://github.com/NekR/offline-plugin ? -->

## Goals

The goal with the application is to get a greater knowledge in both **BE** and **FE**:

**BE**

[**BE** goals can be find here](https://gitlab.com/hiq-kna/cert-backend#goals)

**FE**:

[**FE** goals can be find here](https://gitlab.com/hiq-kna/cert-frontend#goals)

## Dragon eggs (Bonus points)

- Start list, get recently watched and created **cert**
- Show submitted **forms**
- Authentication (User handling)
- native application
